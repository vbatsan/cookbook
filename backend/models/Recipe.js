const {Schema, model} = require('mongoose')

const recipeSchema = new Schema({
    title: {
        type: String,
        required: true,
        min: 3
    },

    descr: {
        type: String,
        required: true,
        min: 100
    },

    date: {
        type: Date,
        default: Date.now
    },

    history: [{
        title: String,
        descr: String,
        date: {
            type: Date,
            default: Date.now
        }
    }]
})

module.exports = model('Recipe', recipeSchema)