const Validator = require('validator');
const isNoItem = require('./is-empty')

module.exports = function validateLoginInputs(data) {

    const errors = {}


    if(Validator.isEmpty(data.title)) {
        errors.title = 'Заповніть поле';
    }

    if(!Validator.isLength(data.title, {min: 3, max: 30})) {
        errors.title = 'Поле має містити від 3 до 30 символів';
    }

    if(Validator.isEmpty(data.descr)) {
        errors.descr = 'Заповніть поле';
    }

    if(!Validator.isLength(data.descr, {min: 20, max: 400})) {
        errors.descr = 'Поле має містити від 20 до 400 символів';
    }

    return {
        errors,
        isValid: isNoItem(errors)
    }
}