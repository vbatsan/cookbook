const router = require("express").Router();
const Recipe = require("../models/Recipe");
const validateInputs = require('../validator/recipe')

router.put("/:name", async (req, res) => {
    const {errors, isValid} = validateInputs(req.body)
    if(!isValid) {
        return res.send(errors)
    }
    const matchRecipe = await Recipe.findOne({title: req.body.title})
    if(matchRecipe) {
        return res.send({status: 'error', message: 'Така назва вже є в базі'})
    }
  const title = req.params.name;
  await Recipe.findOneAndUpdate(
    { title },
    {
      title: req.body.title,
      descr: req.body.descr,
      $push: {
        history: {
          title: req.body.title,
          descr: req.body.descr
        }
      }
    })
  res.send({ status: "success", message: "Дані успішно оновлені" });
});

module.exports = router;
