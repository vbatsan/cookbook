const router = require('express').Router()
const Recipe = require('../models/Recipe')

router.get('/:name', (req, res) => {
    const target = req.params.name
    Recipe.findOne({title: target}, (err, result) => {
        if(err) {
           return res.send(err)
        } else {
            res.send(result)
        }
    })

})

module.exports = router