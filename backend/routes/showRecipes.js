const router = require('express').Router()

const Recipe = require('../models/Recipe')

router.get('/', async (req, res) => {
   try{
     const recipe = await Recipe.find()
    return res.send(recipe)
    } catch(error) {
        res.send(error)
    }
})

module.exports = router