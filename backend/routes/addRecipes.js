const router = require('express').Router()
const validateInputs = require('../validator/recipe')
const Recipe = require('../models/Recipe')

router.post('/', async (req, res) => {
    const {title, descr} = req.body
    const {errors, isValid} = validateInputs(req.body)
    if(!isValid) {
        return res.send(errors)
    }
    try{
        Recipe.findOne({title})
        .then(async (result) => {
            if(result) {
                
                return res.send({status: "error", message: "Такий рецепт вже існує"})
            }

            const recipe = await new Recipe({
                title,
                descr,
                history: [{
                    title,
                    descr,
                }]
            })
            await recipe.save()
            return res.send({status: "success", message: "Рецепт успішно добавлено", recipes: recipe})
        })
       
    } catch(error) {
        console.log(error)
    }
   

})

module.exports = router