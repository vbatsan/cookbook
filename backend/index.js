const express = require('express')
const app = express()
const mongoose = require('mongoose')
const addRecipe = require('./routes/addRecipes')
const getRecipe = require('./routes/showRecipes')
const upgateRecipe = require('./routes/updateReciepes')
const getHistory = require('./routes/getHistory')
const bodyParser = require('body-parser')
const cors = require('cors')
require('dotenv').config()

const port = process.env.PORT || 3002

mongoose.connect(process.env.DATA_BASE, 
    {   useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false 
    }, () => console.log("connected to db")
)

app.use(cors())
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

//Routes
app.use('/recipes', addRecipe)
app.use('/recipes', getRecipe)
app.use('/recipes', upgateRecipe)
app.use('/recipes', getHistory)


app.listen(port, () => console.log(`port started on port ${port}`))
