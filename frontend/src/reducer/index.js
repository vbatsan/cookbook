import { GET_REQUEST, REQUEST_SUCCSESS, GET_ERROR, LIST_SUCCESS, UPDATE_RECIPE, CLEAR_MESSAGE } from '../actions/types';

const initialState = {
    recipes: [],
    error: false,
    isLoading: false,
    message: {}
}

export default function(state = initialState, action ) {
    switch(action.type) {
        case GET_REQUEST:
            return {
                ...state,
                error: false,
                isLoading: true
            }

        case REQUEST_SUCCSESS:
            return {
                ...state,
                isLoading: false,
                message: action.payload,
                recipes: [action.payload.recipes, ...state.recipes]
            }

        case LIST_SUCCESS:
            return{
                ...state,
                isLoading: false,
                recipes: action.payload
            }

            case CLEAR_MESSAGE:
                return{
                    ...state,
                    message: {}
                }

        case UPDATE_RECIPE:
            return{
                ...state,
                isLoading: false,
                message: action.payload
            }

        case GET_ERROR:
            return {
                ...state,
                error: true,
                isLoading: false,
                message: action.payload
            }
        default: 
            return state;
    }
}