import React from 'react'
import {connect} from 'react-redux'

import {updateRecipe} from '../../../actions/list'
import Form from '../../Form'
import History from '../../History'
import moment from 'moment'

class Card extends React.Component {

    render() {
        const {isLoading} = this.props
        return(
            <div className="card mb-3">
                <div className="card-header bg-teal">
                    {this.props.title}
                </div>
                <div className="card-body">
                    <blockquote className="blockquote mb-0">
                        <p>{this.props.descr}</p>
                        <p>{moment(this.props.date).format('lll')}</p>
                    <button className='btn btn-dark'  data-toggle="modal" data-target={`#upd${this.props.title}`}>Редагувати</button>
                    <button className='btn ml-4 btn-info'  data-toggle="modal" data-target={`#${this.props.title}`}>Історія</button>
                    {isLoading && <span>Loading...</span>}
                    <Form title={this.props.title} modalId = {`upd${this.props.title}`} actionFn = {this.props.updateRecipe} role = 'update'/>
                    <History name={this.props.title} modalId={this.props.title} key={this.props.title}/>
                    </blockquote>
                </div>
            </div>
        )
    }
   
}

const mapStateToProps = (state) =>({
    error: state.error,
    isLoading: state.isLoading,
    message: state.message
})



export default connect( mapStateToProps, {updateRecipe})(Card)
