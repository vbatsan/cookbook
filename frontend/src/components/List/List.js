import React from 'react'
import {connect} from 'react-redux'


import {getRecipes} from '../../actions/list'
import Card from './Card'

class List extends React.Component {

    componentDidMount() {
        this.props.getRecipes()
    }

    render() {
        const {list, isLoading} = this.props
        return(
            <div className='bg-secondary py-4'>
                 {isLoading && <div className='w-100 h-75 d-flex justify-content-center align-items-center'><h4>loading...</h4></div>}
                 {(list.length === 0 && !isLoading ) && <div className='w-100 h-75 d-flex justify-content-center align-items-center'><h4>Поки що тут пусто. Додайте свій перший рецепт</h4></div>}
                 {list.map(item => {
                     return <Card title = {item.title} descr = {item.descr} date={item.date} key = {item._id } />
                 })}
            </div>
           

        )
    }


}

const mapStateToProps = state => ({
    isLoading: state.isLoading,
    list: state.recipes,
    error: state.errors
})

export default connect(mapStateToProps,{getRecipes})(List)