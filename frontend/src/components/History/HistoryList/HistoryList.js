import React from 'react'
import moment from 'moment'

export default function HistoryList(props) {
    return(
        <div className='mt-3 border-3 border-primary rounded'>
            <h3>{props.title}</h3>
            <p>{props.descr}</p>
            <span>{moment(props.date).format('lll')}</span>
        </div>
    )
}