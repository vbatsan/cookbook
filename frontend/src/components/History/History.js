import React from "react"
import axios from 'axios'

import HistoryList from './HistoryList'

export default class History extends React.Component {

    state = {
        history:[],
    }

    componentDidMount() {
        axios.get(`http://localhost:3002/recipes/${this.props.name}`)
        .then(res => {
            const sorted = res.data.history.sort((a, b) => {
                if(a.date > b.date) return -1
            })
           this.setState({history: sorted})
        })
    }

    render() {
        return(
            <div className="modal fade" id={this.props.modalId} tabIndex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div className="modal-dialog modal-dialog-scrollable" role="document">
    <div className="modal-content">
      <div className="modal-header">
        <h5 className="modal-title" id="exampleModalScrollableTitle">Історія</h5>
        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div className="modal-body">
            {this.state.history.map(item => {
                return <HistoryList title={item.title} descr={item.descr} date={item.date} key={item._id}/>
            })}
      </div>
    </div>
  </div>
</div>
        )
    }
}