import React from 'react'
import{ connect} from 'react-redux'
import classnames from 'class-names'

class Form extends React.Component {

    state = {
        title: '',
        descr: ''
    }

    handleInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit(e) {
        e.preventDefault()
        const recipe = {
            title: this.state.title,
            descr: this.state.descr
        }
        this.props.actionFn(recipe)

    }

    handleOnUpdate(e) {
        e.preventDefault()
        const target = e.target.dataset.name
        const data = {
            title: this.state.title,
            descr: this.state.descr
        }
        this.props.actionFn(data, target)
    }

    render(){
        const {message, isLoading} = this.props
    return(
            <div className="modal fade" id={this.props.modalId} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLabel">{this.props.role === 'update' ? 'Оновити дані' : 'Новий рецепт'}</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body">
                <form onSubmit={this.props.role === 'update' ? this.handleOnUpdate.bind(this) : this.handleSubmit.bind(this)} data-name={this.props.title}>
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Назва</label>
                        <input type="text" 
                        className={classnames('form-control form-control-lg', {
                            'is-invalid': message.title
                        })}
                         id="exampleInputEmail1"  
                        placeholder="Назва рецепту" 
                        value={this.state.title}
                        name='title' 
                        onChange={this.handleInputChange.bind(this)}/>
                         {message.title && (<div className="invalid-feedback">{message.title}</div>)}
                    </div>
                    <div className="form-group">
                        <label htmlFor="exampleFormControlTextarea1">Рецепт</label>
                        <textarea 
                        className={classnames('form-control form-control-lg', {
                            'is-invalid': message.descr
                        })} 
                        id="exampleFormControlTextarea1" rows="3"
                        value={this.state.descr}
                        name='descr'
                        onChange={this.handleInputChange.bind(this)}>
                        </textarea>
                        {message.descr && (<div className="invalid-feedback">{message.descr}</div>)}
                    </div>
                    <button type="submit" className="btn btn-primary" >{this.props.role === 'update' ? 'Оновити' : 'Додати'}</button>
                    
                    {isLoading && <span>Loading...</span>}
                    {message.status === 'error' && (
                        <span className="alert alert-danger ml-3" role="alert">
                            {message.message}
                        </span>
                    )}
                     {message.status === 'success' && (
                        <span className="alert alert-success ml-3" role="alert">
                            {message.message}
                        </span>
                    )}
                    
                </form>
                </div>
                <div className="modal-footer">
                </div>
                </div>
            </div>
            </div>
           
        )
    }
}

const mapStateToProps = (state) =>({
    error: state.error,
    isLoading: state.isLoading,
    message: state.message
})

export default connect(mapStateToProps)(Form)