import React from 'react'
import { addRecipe } from '../../actions/list'
import{ connect} from 'react-redux'

import Form from '../Form'
class Header extends React.Component {

    render() {
        return(
            <nav className="navbar navbar-light bg-dark">
                <a className="navbar-brand text-white">CookBook</a>
                <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Додати рецепт
                </button>
                <Form modalId='exampleModal' actionFn = {this.props.addRecipe} />
            </nav>
        )
    }
   
}

export default connect(null, {addRecipe})(Header)
    