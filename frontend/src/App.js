import React from 'react';
import 'jquery'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/js/dist/modal'
import {Provider} from 'react-redux'
import store from './store'


import List from './components/List'
import Header from './components/Header'

function App() {
  return (
    <Provider store={store}>
      <Header/>
      <List/>
    </Provider>
  )
}
   

export default App;
