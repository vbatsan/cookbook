import {GET_REQUEST, REQUEST_SUCCSESS, GET_ERROR, LIST_SUCCESS, UPDATE_RECIPE, CLEAR_MESSAGE} from './types'
import axios from 'axios'

const clearMessage = () => ({
    type: CLEAR_MESSAGE
})

export const addRecipe = (data) => dispatch => {
    dispatch({
        type: GET_REQUEST
    })
    axios.post('http://localhost:3002/recipes', data)
    .then(res => {
        if(res.data.status === 'success'){
           dispatch({
                type: REQUEST_SUCCSESS,
                payload: res.data
            })
        } else {
            dispatch({
                type:GET_ERROR,
                payload: res.data
            })
        }
        setTimeout(() =>dispatch(clearMessage()), 2000)
       
    })
    .catch((error) => {
        dispatch({
            type:GET_ERROR,
            payload: error
        })
    })
}

export const getRecipes = () => dispatch => {
    dispatch({
        type: GET_REQUEST
    })
    axios.get('http://localhost:3002/recipes')
    .then(res => {
        const filtered = res.data.sort((a, b) => {
            if(a.date > b.date) return -1
        })
        dispatch({
            type: LIST_SUCCESS,
            payload: filtered
        })
    }).catch(error => {
        dispatch({
            type: GET_ERROR,
            payload: error
        })
    })
}



export const updateRecipe = (data, target) => dispatch => {
    dispatch({type: GET_REQUEST})
    axios.put(`http://localhost:3002/recipes/${target}`, data)
    .then(res => {
        console.log(res.data)
        if(res.data.status === 'success'){
            dispatch({
                type: UPDATE_RECIPE,
                payload: res.data
            })
           dispatch(getRecipes()) 
        } else{
            dispatch({
                type: GET_ERROR,
                payload: res.data
            })
        }
        setTimeout(() =>dispatch(clearMessage()), 2000)
       
    }).catch(error => {
        dispatch({
            type: GET_ERROR,
            payload: error
        })
    })
}

